import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardHistoryComponent } from '../app/pages/card-history/card-history.component'


const routes: Routes = [
  {
    path: "card-history",
    component: CardHistoryComponent
  },
  {
    path: "",
    redirectTo: "card-history",
    pathMatch: 'full'
  },
  {
    path: "**",
    redirectTo: "card-history",
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
