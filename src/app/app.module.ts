import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material-module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CardHistoryComponent } from './pages/card-history/card-history.component';
import { GeneralExpensesComponent } from './pages/general-expenses/general-expenses.component';
import { MonthExpensesComponent } from './pages/month-expenses/month-expenses.component';
import { CategoryExpensesComponent } from './pages/category-expenses/category-expenses.component';
import { HeaderComponent } from './common/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    CardHistoryComponent,
    GeneralExpensesComponent,
    MonthExpensesComponent,
    CategoryExpensesComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
