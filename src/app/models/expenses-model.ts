export interface Expenses {
    id: number,
    valor: number,
    origem: string,
    categoria: number,
    mes_lancamento: number
};