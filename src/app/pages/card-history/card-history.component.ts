import { Component, OnInit } from '@angular/core';
import { ExpensesServiceService } from 'src/app/services/expenses-service.service';
import { ThrowStmt } from '@angular/compiler';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material/dialog';

@Component({
  selector: 'app-card-history',
  templateUrl: './card-history.component.html',
  styleUrls: ['./card-history.component.scss']
})
export class CardHistoryComponent implements OnInit {

  expenses: any;
  generalExpenses: any;
  monthExpenses: any;
  categoryExpenses: any;
  everyCategories: any;
  showLoader: boolean = true;

  constructor(private expenseService: ExpensesServiceService) { }

  ngOnInit(): void {
    this.getEveryExpenses();
  };

  async getEveryExpenses() {
    this.expenses = await this.expenseService.getExpenses();
    this.everyCategories = await this.expenseService.getCategories();
    this.showLoader = false;
    this.loadGeneralTab(this.expenses)
  };

  loadGeneralTab(expenses) {
    this.generalExpenses = expenses.sort((a, b) => a.mes_lancamento - b.mes_lancamento);
    this.loadMonthTab(expenses);
  };

  loadMonthTab(expenses) {
    this.monthExpenses = expenses.reduce((acumulador, cur) => {
      if (!cur.valor) return acumulador;

      const foundMonths = acumulador.find(e => e.mes_lancamento === cur.mes_lancamento);

      if (foundMonths) {
        foundMonths.valor += cur.valor
      } else {
        acumulador.push({ mes_lancamento: cur.mes_lancamento, valor: cur.valor })
      }

      return acumulador
    }, []).sort((a, b) => a.mes_lancamento - b.mes_lancamento);

    this.loadCategoryTab(expenses);
  };

  loadCategoryTab(expenses) {
    this.categoryExpenses = expenses.reduce((acumulador, cur) => {
      if (!cur.valor) return acumulador;

      const foundCategories = acumulador.find(e => e.categoria === cur.categoria);

      if (foundCategories) {
        foundCategories.valor += cur.valor
      } else {
        acumulador.push({ categoria: cur.categoria, valor: cur.valor })
      }

      return acumulador
    }, []).sort((a, b) => a.categoria - b.categoria);

  };
 
}
