import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-category-expenses',
  templateUrl: './category-expenses.component.html',
  styleUrls: ['./category-expenses.component.scss']
})
export class CategoryExpensesComponent implements OnInit {

  @Input()
  data: [];

  @Input()
  categories: any;

  constructor() { }

  ngOnInit(): void {
  }

  formatNumberInBrlCurrency(number) {
    return number.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
  };

  getCategoryLabeL(cateogiryId) {
    return this.categories.find(e => e.id === cateogiryId).nome;
  };

}
