import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-general-expenses',
  templateUrl: './general-expenses.component.html',
  styleUrls: ['./general-expenses.component.scss']
})
export class GeneralExpensesComponent implements OnInit {

  @Input()
  data;

  constructor() { }

  ngOnInit(): void {
  }

  formatNumberInBrlCurrency(number) {
    return number.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
  };

}
