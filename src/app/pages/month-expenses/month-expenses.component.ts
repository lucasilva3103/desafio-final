import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-month-expenses',
  templateUrl: './month-expenses.component.html',
  styleUrls: ['./month-expenses.component.scss']
})
export class MonthExpensesComponent implements OnInit {

  @Input()
  data;

  constructor() { }

  ngOnInit(): void {
  }

  formatNumberInBrlCurrency(number) {
    return number.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
  };

  
  getMonthLabel(month) {
    switch (month) {
      case 1:
        return "Janeiro"
      case 2:
        return "Fevereiro"
      case 3:
        return "Março"
      case 4:
        return "Abril"
      case 5:
        return "Maio"
      case 6:
        return "Junho"
      case 7:
        return "Julho"
      case 8:
        return "Agosto"
      case 9:
        return "Setembro"
      case 10:
        return "Outubro"
      case 11:
        return "Novembro"
      case 12:
        return "Dezembro"
    };
  };

}
