import { Injectable } from '@angular/core';
import axios from "axios";
import { environment } from "src/environments/environment";
import { Expenses } from '../models/expenses-model'
import { Categories } from '../models/categories-model'

@Injectable({
  providedIn: 'root'
})
export class ExpensesServiceService {

  constructor() { }

  async getExpenses(): Promise<Array<Expenses>> {
    const response = await axios.get(`${environment.baseUrl}/lancamentos`);
    return response.data
  };

  async getCategories(): Promise<Array<Categories>> {
    const response = await axios.get(`${environment.baseUrl}/categorias`);
    return response.data
  };

}
